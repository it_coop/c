﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Praca_domowa
{
    class Program
    {
        static void Main(string[] args)
        {
            // Zad 1. Rozwiązać równanie liniowe ax+b=0 
            int a, b;
            Console.WriteLine("Podaj a:");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj b:");
            b = int.Parse(Console.ReadLine());
            if (a == 0)
                if (b == 0)
                    Console.WriteLine("Równanie tożsamościowe");
                else
                    Console.WriteLine("Równanie sprzeczne");
            else
                Console.WriteLine("Rozwiązanie równania: " + (float)-b / a);
            

            // Zad 2. Rozwiązać równanie kwadratowe ax^2+bx+c=0. Zakładamy, że a != 0;
            int a, b, c, delta;
            Console.WriteLine("Podaj a:");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj b:");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj c:");
            c = int.Parse(Console.ReadLine());
            delta = (b * b) - (4 * a * c);
            if (delta > 0)
                Console.WriteLine("x1: " + (float)(-b - Math.Sqrt(delta)) / 2 * a + "\n x2: " + (float)(-b + Math.Sqrt(delta)) / 2 * a);
            else if (delta == 0)
                Console.WriteLine("x: " + (float)-b / 2 * a);
            else
                Console.WriteLine("Brak pierwiastków.");
            

            // Zad 3. Policzyć sumę liczb od a do b, lub od b do a 
            int a, b, suma = 0;
            Console.WriteLine("Podaj a:");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj b:");
            b = int.Parse(Console.ReadLine());
            if (a < b)
                for (int i = a; i <= b; i++)
                    suma += i;
            else
                for (int i = b; i <= a; i++)
                    suma += i;
            Console.WriteLine(suma);
            

            // Zad 4. Policzyć sumę ciągu wczytywanych liczb zakończonego zerem
            int l, suma = 0;
            do
            {
                Console.WriteLine("Podaj liczbę do sumowania. 0 kończy działanie");
                l = int.Parse(Console.ReadLine());
                suma += l;
            }
            while (l != 0);
            Console.WriteLine(suma);
            

            // Zad 5. Wypisać liczby parzyste od a do b, lub od b do a
            int i;
            Console.WriteLine("Podaj a:");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj b:");
            int b = int.Parse(Console.ReadLine());
            if (a < b)
                for (i = a; i <= b; i++)
                    if (i % 2 == 0)
                        Console.WriteLine(i);
            if (a > b) 
                for (i = a; i >= b; i--)
                    if (i % 2 == 0)
                        Console.WriteLine(i);

            // Zad 6. Policzyć silnię z liczby a;
            Console.WriteLine("Podaj a:");
            int a = int.Parse(Console.ReadLine());
            if (a == 0)
                Console.WriteLine(0);
            else
            {
                int silnia = 1;
                for (int i = 2; i <= a; i++)
                    silnia *= i;
                Console.WriteLine(silnia);
            }
            

            // Zad 7. Wypisać liczby podzielne przez 3 z zakresu od a do b lub od b do a 
            int i;
            Console.WriteLine("Podaj a:");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj b:");
            int b = int.Parse(Console.ReadLine());
            if (a < b)
                for (i = a; i <= b; i++)
                    if (i % 3 == 0)
                        Console.WriteLine(i);
            if (a > b)
                for (i = a; i >= b; i--)
                    if (i % 3 == 0)
                        Console.WriteLine(i);
            Console.ReadKey();
        }
    }
}
